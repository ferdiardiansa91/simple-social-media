import React, { Component } from 'react';
import { withRouter } from 'react-router-dom';

import WebRouter from 'routes/WebRouter';

export class App extends Component {

	render() {
		return(<div>
			<div>
				<WebRouter />
			</div>
		</div>)
	}
}

export default withRouter(App);

